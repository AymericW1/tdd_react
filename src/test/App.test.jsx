import React from "react";
import App from "../App";
import { fireEvent, render, screen } from "@testing-library/react";
import "@testing-library/jest-dom/";


describe("Creating a message", () => {
  test("Displays the message in the list", () => {
    render(<App/>)
    const messageText = screen.getByTestId("messageText");
    fireEvent.input(messageText, "New message");
    const sendButton = screen.getByTestId("sendButton");
    fireEvent.click(sendButton);
    expect(messageText).toHaveValue("");
  });

});
