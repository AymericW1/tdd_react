import "./App.css";
import NewMessageForm from "../src/components/NewMessageForm/index";
import MessageList from "./components/MessageList/index";
import { useState } from "react";

export default function App() {
  const [messages, setMessages] = useState([]);

  function handleSend(newMessage) {
    setMessages([newMessage, ...messages]);
  }
  return (
    <>
      <NewMessageForm onSend={handleSend} />
      <MessageList data={messages} />
    </>
  );
}
